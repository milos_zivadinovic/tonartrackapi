﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence.Helpers
{
    public abstract class AbstractStatusHolder
    {
        public string Status { get; set; }
    }
}
