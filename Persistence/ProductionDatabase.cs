﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Persistence.Contexts;
using Persistence.Enums;
using Persistence.Interfaces;
using Persistence.Tables;

namespace Persistence
{
    public class ProductionDatabase : IDatabaseFacade
    {
        private TonarTrackContext _context;
        private static ProductionDatabase _instance;

        private ProductionDatabase()
        {
            _context = new TonarTrackContext();
        }

        public static ProductionDatabase Instance => _instance ??= new ProductionDatabase();

        //public IEnumerable<Teacher> GetTeachers() => _context.Teachers != null ? _context.Teachers : new List<Teacher>() { new Teacher(EStatus.MISSING) };
        public IEnumerable<Teacher> GetTeachers() => _context.Teachers;

        public Teacher GetTeacher(long id) => _context.Teachers.Find(id) ?? new Teacher(EStatus.MISSING);
    }
}
