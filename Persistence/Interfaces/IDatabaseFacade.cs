﻿using System;
using System.Collections.Generic;
using System.Text;
using Persistence.Tables;

namespace Persistence.Interfaces
{
    public interface IDatabaseFacade
    {
        IEnumerable<Teacher> GetTeachers();
        Teacher GetTeacher(long id);
    }
}
