﻿using Persistence.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Persistence.Tables
{
    public class Teacher
    {
        public Teacher()
        {

        }

        public Teacher(EStatus objectStatus) => Status = objectStatus;      // We use this to emulate null objects and also package up short error responses for the DTO

        public long Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public List<Teacher> ToList() => new List<Teacher>() { this };

        [NotMapped]
        public EStatus Status { get; set; } = EStatus.FOUND;
    }
}
