﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace BusinessLogic.Interfaces.Common
{
    public interface IComposite
    {
        IEnumerable<T> Cast<T>();

        IComposite GetAll();
        IComposite GetOne(long id);
    }
}