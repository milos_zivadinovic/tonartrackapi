﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using Persistence;
using Persistence.Interfaces;
using Serilog;

namespace BusinessLogic.Interfaces.Common
{
    public interface IFactory
    {
        IComposite Create<T>();
        void SetMapper(IMapper mapper);
        void SetDatabase(IDatabaseFacade database);
        void SetLogger(ILogger logger);
    }
}
