﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.Interfaces.Teacher;

namespace BusinessLogic.Interfaces.Common
{
    public interface IService
    {
        public IQuery Query();
        public ICommand Command();
    }
}
