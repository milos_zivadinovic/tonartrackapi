﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Interfaces.Common
{
    public interface IQuery
    {
        public IComposite GetAll();
        public IComposite GetOne(long id); 
    }
}
