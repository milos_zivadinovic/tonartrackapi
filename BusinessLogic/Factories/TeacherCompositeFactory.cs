﻿using System;
using System.Collections.Generic;
using System.Text;
using AutoMapper;
using BusinessLogic.Composites;
using BusinessLogic.Interfaces.Common;
using BusinessLogic.Interfaces.Teacher;
using Persistence;
using Persistence.Interfaces;
using Serilog;

namespace BusinessLogic.Factories
{
    public class TeacherCompositeFactory : IFactory
    {
        private static TeacherCompositeFactory _instance;

        public static TeacherCompositeFactory Instance
        {
            get { return _instance ??= new TeacherCompositeFactory(); }
        }

        private IMapper _mapper;
        private IDatabaseFacade _database;
        private ILogger _logger;

        public void SetMapper(IMapper mapper) => _mapper = mapper;
        public void SetDatabase(IDatabaseFacade database) => _database = database;
        public void SetLogger(ILogger logger) => _logger = logger;

        public IComposite Create<T>()
        {
            IComposite result = null;

            try
            {
                Type teacherType = typeof(T);

                switch (teacherType.Name) // TODO: make some kind of string mapping or whatever on application startup
                {
                    case("Teacher"):
                        result = new TeacherComposite(_mapper, _database, _logger);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception exception)
            {
                
            }

            return result;
        }
    }
}
