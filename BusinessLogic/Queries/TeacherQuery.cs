﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using BusinessLogic.Composites;
using BusinessLogic.Factories;
using BusinessLogic.Interfaces.Common;
using BusinessLogic.Interfaces.Teacher;
using Persistence;
using Persistence.Interfaces;
using Persistence.Tables;
using Serilog;

namespace BusinessLogic.Queries
{
    public class TeacherQuery : IQuery
    {

        public TeacherQuery(IDatabaseFacade database, IMapper mapper, ILogger logger)    // TODO: Use AutoFac (or some other dependency injector) instead of constructor injection?
        {
            TeacherCompositeFactory.Instance.SetMapper(mapper);
            TeacherCompositeFactory.Instance.SetDatabase(database);
            TeacherCompositeFactory.Instance.SetLogger(logger);
        }

        public IComposite GetAll() => TeacherCompositeFactory.Instance.Create<Teacher>().GetAll();
        public IComposite GetOne(long id) => TeacherCompositeFactory.Instance.Create<Teacher>().GetOne(id);
    }
}
