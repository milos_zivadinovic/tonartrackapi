﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLogic.Commands;
using BusinessLogic.DTOs;
using BusinessLogic.Interfaces.Common;
using BusinessLogic.Interfaces.Teacher;
using BusinessLogic.Queries;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Interfaces;
using Persistence.Tables;
using Serilog;
using Serilog.Sinks.SystemConsole.Themes;

namespace TonarTrackAPI.Services
{
    public class TeacherService : ITeacherService
    {
        private readonly IDatabaseFacade _database;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;

        public TeacherService()
        {
            
            _database = ProductionDatabase.Instance;
            
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Teacher, TeacherDTO>();
            }); 

            _mapper = config.CreateMapper();
            _logger = new LoggerConfiguration()
                .Enrich.FromLogContext()
                .Enrich.WithThreadId()
                .Enrich.WithThreadName()
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] <{ThreadId}><{ThreadName}> {Message:lj} {NewLine}{Exception}")
                .WriteTo.File("business-.log",
                    outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] <{ThreadId}><{ThreadName}> {Message:lj} {NewLine}{Exception}",
                    rollingInterval: RollingInterval.Day)
                .CreateLogger();
        }

        public IQuery Query()
        {
            IQuery result = null;

            try
            {
                result = new TeacherQuery(_database, _mapper, _logger);
            }
            catch (Exception exception)
            {
                _logger.Error(exception, exception.Message);
            }

            return result;
        }

        public ICommand Command()
        {
            ICommand result = null;

            try
            {
                result = new TeacherCommand(_database);
            }
            catch (Exception exception)
            {
                _logger.Error(exception, exception.Message);
            }

            return result;
        }
    }
}
