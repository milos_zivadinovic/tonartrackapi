﻿using AutoMapper;
using BusinessLogic.Interfaces.Common;
using Persistence.Interfaces;
using Persistence.Tables;
using System.Collections.Generic;
using System.Linq;
using Serilog;

namespace BusinessLogic.Composites
{
    public class TeacherComposite : IComposite
    {
        private readonly IMapper _mapper;
        private readonly IDatabaseFacade _database;
        private readonly ILogger _logger;

        private readonly IEnumerable<Teacher> _teachers;

        public TeacherComposite(IMapper mapper, IDatabaseFacade database, ILogger logger)
        {
            _mapper = mapper;
            _database = database;
            _logger = logger;
        }

        private TeacherComposite(IEnumerable<Teacher> teachers, IMapper mapper, IDatabaseFacade database, ILogger logger) : this(mapper, database, logger)
        {
            _teachers = teachers;
        }

        public IEnumerable<T> Cast<T>() => _teachers.Select(teacher => _mapper.Map<T>(teacher)).ToList();

        public IComposite GetAll() => new TeacherComposite(_database.GetTeachers(), _mapper, _database, _logger);

        public IComposite GetOne(long id) => new TeacherComposite(_database.GetTeacher(id)?.ToList(), _mapper, _database, _logger);
    }
}
