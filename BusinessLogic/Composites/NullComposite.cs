﻿using BusinessLogic.Interfaces.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessLogic.Composites
{
    public class NullComposite : IComposite
    {
        private NullComposite()
        {

        }

        private static NullComposite _instance;
        public static NullComposite Instance => _instance ??= new NullComposite();

        public IEnumerable<T> Cast<T>() => null;

        public IComposite GetAll() => null;

        public IComposite GetOne(long id) => null;

        public IComposite Guard(IComposite composite) => composite ?? Instance;
    }
}
