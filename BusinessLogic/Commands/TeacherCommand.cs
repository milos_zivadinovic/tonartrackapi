﻿using System;
using System.Collections.Generic;
using System.Text;
using BusinessLogic.Interfaces.Common;
using BusinessLogic.Interfaces.Teacher;
using Persistence;
using Persistence.Interfaces;

namespace BusinessLogic.Commands
{
    public class TeacherCommand : ICommand
    {
        private IDatabaseFacade database;

        public TeacherCommand(IDatabaseFacade databaseToUse)
        {
            database = databaseToUse;
        }
    }
}
