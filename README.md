# TonarTrackAPI
TonarTrack is an application that would allow tutors to organize and customize lectures for their individual students. This is a very strong ***Work in Progress*** since I always wanted to have a toy project of my own. 

At this point, it might not even build correctly (see technical details below), depending on commit. It should have one model - Teacher, that I am using as a tracer to develop the whole application. Afterwards it will be much simpler to add more models and bind them together.

## Technical details

The purpose of this project is to have a playground so I can test concepts learned from Pluralsight, as well as a way to experiment with current .NET technologies.

I also wanted to make a project that uses DDD as much as possible. So far it is going well, but there is a lot of formal stuff that I need to learn.

### Technologies currently used:
* .NET Core
* ASP.NET Core
* EF Core
* Autofac
* Serilog
* AutoMapper
* Postgres

### Technologies that were used:
* RavenDB - I was unable to register for a license, but I really liked it

### Technologies that will probably get used:
* React
* Docker - this is a perfect opportunity to create a Dockerized application which is a bit more complex than a regular Hello World application
* Kubernetes - let's kick it up a notch and make it as distributed as possible

### Potential refactorings in the future:
* Event sourcing - didn't want to do it from the beginning
* Some logic and stuff will be moved onto the database layer, to keep the separation of concerns 