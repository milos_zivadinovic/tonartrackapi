﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BusinessLogic.DTOs;
using BusinessLogic.Interfaces.Common;
using BusinessLogic.Interfaces.Teacher;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TonarTrackAPI.Helpers;

namespace TonarTrackAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeacherController : ControllerBase
    {

        private readonly ILogger<TeacherController> logger;
        private readonly IService dataProvider;    

        public TeacherController(ILogger<TeacherController> injectedLogger,
            ITeacherService teacherService)
        {
            logger = injectedLogger;
            dataProvider = teacherService;
        }

        [HttpGet]
        public IEnumerable<TeacherDTO> Get()
        {
            IEnumerable<TeacherDTO> result = null;

            try
            {
                result = dataProvider   // TODO: extend to handle IEnumerable; might have some architectural changes also
                    .Query()
                    .GetAll()
                    .Cast<TeacherDTO>();
            }
            catch (Exception exception)
            {
                logger.LogError(exception.Message, exception);
            }

            return result;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(long id)
        {
            IActionResult result = default;

            try
            {
                result = HttpResultFactory.Instance.Build(dataProvider
                    .Query()
                    .GetOne(id)
                    .Cast<TeacherDTO>()
                    .First());
            }
            catch (Exception exception)
            {
                logger.LogError(exception.Message, exception);
            }

            return result;
        }
    }
}
