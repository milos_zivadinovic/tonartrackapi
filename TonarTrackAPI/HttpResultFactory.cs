﻿using Microsoft.AspNetCore.Mvc;
using Persistence.Enums;
using Persistence.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace TonarTrackAPI.Helpers
{
    public class HttpResultFactory
    {
        private static HttpResultFactory _instance;

        public static HttpResultFactory Instance
        {
            get { return _instance ??= new HttpResultFactory(); }
        }

        public IActionResult Build(AbstractStatusHolder item)
        {
            IActionResult result = default;   // This is the default branch, so we're omitting it in the switch

            try
            {
                Enum.TryParse(item.Status, out EStatus parsedStatus);

                switch (parsedStatus)
                {
                    case EStatus.FOUND:
                        result = new OkObjectResult(item);
                        break;
                    case EStatus.MISSING:
                        result = new NotFoundObjectResult(item);
                        break;
                    default:
                        result = new StatusCodeResult(500);
                        break;
                }
            }
            catch (Exception)
            {

            }

            return result;
        }


    }
}
